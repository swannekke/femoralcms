function fill(last)
{
  if(typeof neu === 'undefined')
  {
    neu = last;
  }
  var input1 = document.createElement("input");
  var input2 = document.createElement("input");

  var name1 =document.createAttribute("name");
  var name2 =document.createAttribute("name");

  var type1 =document.createAttribute("type"); 
  var type2 =document.createAttribute("type");
  
  var style1 =document.createAttribute("style");
  
  var trid =document.createAttribute("id");

  name1.nodeValue = neu+"[topic]";
  name2.nodeValue = neu+"[name]";

  trid.nodeValue = neu;

  var text0 = document.createTextNode(neu);

  neu = neu+1;

  type1.nodeValue = "text";
  type2.nodeValue = "text";
  
  style1.nodeValue = "width:20px;";

  input1.setAttributeNode(type1);
  input1.setAttributeNode(name1);
  input1.setAttributeNode(style1);
  
  input2.setAttributeNode(type2);
  input2.setAttributeNode(name2);

  var tr = document.createElement("tr");
  var td0 = document.createElement("td");
  var td1 = document.createElement("td");
  var td2 = document.createElement("td");
  
  var out =document.getElementById("table");

  tr.setAttributeNode(trid);
  td0.appendChild(text0);
  td1.appendChild(input1);
  td2.appendChild(input2);
  
  tr.appendChild(td0);
  tr.appendChild(td1);
  tr.appendChild(td2);
  out.appendChild(tr);
}

function show_login()
{
  $("#form").show();
}

function insertafter(newchild, refchild) { 
 refchild.parentNode.insertBefore(newchild,refchild.nextSibling); 
} 

function UnCryptMailto( s )
{
    var n = 0;
    var r = "";
    for( var i = 0; i < s.length; i++)
    {
        n = s.charCodeAt( i );
        if( n >= 8364 )
        {
            n = 128;
        }
        r += String.fromCharCode( n - 1 );
    }
    return r;
}

function linkTo_UnCryptMailto( s )
{
    location.href=UnCryptMailto( s );
}

$(document).ready(function() {
 
    if ( !("placeholder" in document.createElement("input")) ) {
        $("input[placeholder], textarea[placeholder]").each(function() {
            var val = $(this).attr("placeholder");
            if ( this.value == "" ) {
                this.value = val;
            }
            $(this).focus(function() {
                if ( this.value == val ) {
                    this.value = "";
                }
            }).blur(function() {
                if ( $.trim(this.value) == "" ) {
                    this.value = val;
                }
            })
        });
 
        // Clear default placeholder values on form submit
        $('form').submit(function() {
            $(this).find("input[placeholder], textarea[placeholder]").each(function() {
                if ( this.value == $(this).attr("placeholder") ) {
                    this.value = "";
                }
            });
        });
    }
});