<?php

if(!isset($_GET['step'])) $_GET['step'] = "";
if(!isset($_POST['database_name'])) $_POST['database_name'] = "";

?>

<html>
  <head>
    <title>Femoral Installationsassistent</title>
    <style type="text/css">
      body, html {
      	margin:0;
      	padding:0;
      	height:100%;
      	width:100%;
      	background-color: #999;
      	font-family: sans-serif,verdana,arial;
      }
      body > .box {
      	width:550px;
      	height:400px;
      	margin:10% auto;
      	background-color: #fff;
      	padding:1%;
      	border:1px solid black;
      	box-shadow: 1px 1px 5px black;
      	text-align: center;
      }
    </style>
  </head>
  <body>
  	<div class="box">
		<?php
			if($_GET['step'] == ""){
				?>
					Die Datenbank konnte nicht benutzt werden: 
					<?php

					$error = mysql_error();

					echo $error;

					$error_ex = explode("'", $error);

					?>
					.<br><br>
					Dieser Wizard wird sie durch eine Neuinstallation der Datenbank f&uuml;hren:
					<br><br>
					<form method="post" action="installsql/wizard.php?step=2">
						<input type="hidden" name="database_name" value="<?=$error_ex[1]?>">
						<input value="Datenbank <?=$error_ex[1]?> jetzt neu installieren" type="submit">
					</form>
				<?php
			}
			if($_GET['step'] == "2"){
				$do_not_connect = true;
				include "config.php";
				$conn = mysql_connect($dbhost, $dbuser, $dbpass);
				if(! $conn )
				{
				  echo('Konnte nicht zu Mysql verbinden: ' . mysql_error());
				  exit;
				}
				echo 'Mysql Verbindung erfolgreich<br>';
				$sql = 'CREATE Database '.$_POST['database_name'];
				$retval = mysql_query( $sql, $conn );
				if(! $retval )
				{
				  echo('konnte Datenbank nicht erstellen: ' . mysql_error());
				  exit;
				}
				echo "Datenbank wurde erfolgreich erstellt<br><br>";
				mysql_close($conn);
				?>
				  Vor dem n&auml;chsten Schritt sollten die Standart User in /installsql/daten/users.fill.php angepasst werden.<br>
				  Aus Sicherheitsgr&uuml;nden sollte der test User entfernt werden.<br><br>
				  Im n&auml;chsten Schritt bitte einfach alles anhaken und installieren dr&uuml;cken, danach ist die Installation abgeschlossen:<br><br> <a href="index.php">Weiter</a>
				<?php
			}
		?>
		
	</div>
</body>