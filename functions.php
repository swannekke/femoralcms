<?php

function gen_links($links)
{
  foreach($links as $key => $link)
  {
    $result = mysql_query("SELECT * FROM `projekte` WHERE `file` = '".$key."'");
    $projekt_link = mysql_fetch_assoc($result);
    $zusatz = "";
    $zusatz2 = '';
    if($_GET['seite'] == $key){
      $zusatz = ' class="darklink"';
    }
    if($projekt_link['hide'] == 1){
      $zusatz2 = '<span style="color:red;">(h)</span> ';
    }
    echo '<li><a'.$zusatz.' href="'.$key.'">'.$zusatz2.$link.'</a></li>'."\n";
    if($projekt_link != "")
    {
      if($_SESSION['drin'] != 1)
      {
        $wennnichdrin = " and `text` != '' and `hide` = '0'";
      } else {
        $wennnichdrin = "";
      }
      $result = mysql_query("SELECT * FROM `projekte` WHERE `topic` = '".$projekt_link['id']."'".$wennnichdrin." ORDER BY `pos` ASC");
      while($projekt_link_k = mysql_fetch_assoc($result))
      {
        $subpages[] = $projekt_link_k['file'];
        $subpages_dis[$projekt_link_k['file']] = $projekt_link_k['name'];
        $subpages_hide[$projekt_link_k['file']] = $projekt_link_k['hide'];
      }
      $subpages = isset($subpages) ? $subpages : "";
      if(is_array($subpages))
      {
        foreach($subpages as $projekt_link_k)
        {
          if($_GET['seite'] == $projekt_link['file'] or in_array($_GET['seite'],$subpages))
          {
            $zusatz = "";
            $zusatz2 = '';
            if($_GET['seite'] == $projekt_link_k){
              $zusatz = ' darklink';
            }
            if($subpages_hide[$projekt_link_k] == 1){
              $zusatz2 = '<span style="color:red;">(h)</span> ';
            }
            echo '<li><a class="small'.$zusatz.'" href="'.$projekt_link_k.'">'.$zusatz2.$subpages_dis[$projekt_link_k].'</a></li>'."\n";
          }
        }
        unset($subpages);
      }
    }
  }
}

function makeClickableLinks($s) {
  return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
}

function alter($tag,$mon,$jah){
    $jetzt = mktime(0, 0, 0, date('m'), date('d'), date('Y')); 
   $gebur = mktime(0, 0, 0, $mon, $tag, $jah); 
   return $age   = intval(($jetzt - $gebur) / (3600 * 24 * 365)); 
}

function cutStr($string, $pos) {
    if ($pos < strlen($string)) {
        $text = substr($string, 0, $pos);

        if (false !== ($strrpos = strrpos($text,' '))) {
            $text = substr($text, 0, $strrpos);
        }

        $string = $text . ' [...]';
    }

    return $string;
}

function gen_pw($stellen)
{
  $pw = "";
  $zeichen = 'abcdefghjkmnpqrstvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789';
  $zeichen_count = strlen($zeichen);
  for($i=1;$i<=$stellen;$i++)
  {
    $pw .= $zeichen[rand(0,$zeichen_count-1)];
  }
  return $pw;
}

function get_users($projekt_id)
{
  $result = mysql_query("SELECT * FROM `projekte` WHERE `id` = '".$projekt_id."'");
  $projekt = mysql_fetch_assoc($result);
  $users = explode("|",$projekt['from']);
  foreach($users as $oneuser)
  {
    $result2 = mysql_query("SELECT * FROM `users` WHERE `id` = '".$oneuser."'");
    $user = mysql_fetch_assoc($result2);
    $user_namen[] = $user['name'];
  }
  return(implode(", ",$user_namen));
}

function umlaute($text)
{
  $umlaute = array("ä","ö","ü","Ä","Ö","Ü","ß","€"); 
  $replace = array("ae","oe","ue","Ae","Oe","Ue","ss","Euro");
  return(str_replace($umlaute, $replace, utf8_encode($text)));
}

function umhtml($text)
{
  $umlaute = array("ä","ö","ü","Ä","Ö","Ü","ß","€"); 
  $replace = array("&auml;","&ouml;","&uuml;","&Auml;","&Ouml;","&Uuml;","&szlig;","&euro;"); 
  return(str_replace($umlaute, $replace, utf8_encode($text)));
}

function resize_pic($sourceimage,$width = 760)
{
  $path = pathinfo($sourceimage);

  $info = getimagesize($sourceimage);
  $widthold = $info[0];
  $heightold = $info[1];
  if($widthold > $width)
  {
    $height = ceil($heightold*$width/$widthold);
    switch($info[2])
    {
      case 1: //Gif
        $imageold = imagecreatefromgif($sourceimage);
        break;
      case 2: //Jpeg
        $imageold = imagecreatefromjpeg($sourceimage);
        break;
      case 3: //Png
        $imageold = imagecreatefrompng($sourceimage);
        break;
    }
    $imagenew = imagecreatetruecolor($width, $height);
    imagecopyresampled($imagenew, $imageold, 0, 0, 0, 0, $width, $height, $widthold, $heightold);
    imagejpeg($imagenew,$path["dirname"]."/".$path["filename"].$width.".jpg",95);
    imagedestroy($imageold);
    imagedestroy($imagenew);
    return true;

  }
  else
  {
    return false;
  }
}

//Wandelt BB codes um
function parse($text)
{
  //$text = umhtml($text);
  //$text = utf8_decode($text);
  $text2 = preg_replace("#[\n]#", '',$text);
  
  $text = htmlentities($text,ENT_NOQUOTES);
  
  //code highlighning
  $text = preg_replace_callback("/\[code\](.*?)\[\/code\]/is", create_function('$hits', 'return "<div style=\"padding:5px;border:1px solid black;overflow:auto;background-color:#eee;\">".highlight_string(preg_replace(\'/\[code\](.*?)\[\/code\]/is\', \'\\1\', $hits[0]), 1)."</div>";'), $text2);


  //HTML mit callback. Entfernt auch evt. Zeilenumbrueche
  $text = preg_replace_callback('/\[html\](.*?)\[\/html\]/is', function($match){return preg_replace("#[\r|\n]#", '',html_entity_decode($match[1]));}, $text);

  

  $text = nl2br($text);
  
  //Fett, Kursiv, Unterstrichen und headline umwandeln
  $text = preg_replace('/\[b\](.*?)\[\/b\]/', '<b>$1</b>', $text);
  $text = preg_replace('/\[i\](.*?)\[\/i\]/', '<i>$1</i>', $text);
  $text = preg_replace('/\[u\](.*?)\[\/u\]/', '<u>$1</u>', $text);
  $text = preg_replace('/\[h\](.*?)\[\/h\]/', '<h2>$1</h2>', $text);
  
  $text = preg_replace_callback('/\[pic=(.*?)\]/', 'output_pic', $text);
  //URL's
  $text = preg_replace("/\[url\=(.*?)\](.*?)\[\/url\]/i", "<a target=\"_blank\" href=\"$1\">$2</a>", $text);
  
  //Links automatisch erkennen
  //$text = preg_replace("/([\w]+:\/\/[\w-?&;|#~%=\.\/\@]+[\w\/])/i","<a target=\"_blank\" href=\"$1\">$1</a>",$text);

  return $text;
}

//wird in parse() aufgerufen und erzeugt code fuer bilder
function output_pic($pic)
{
  $default_width = 760;
  $default_width_lightbox = 1000;
  $params = explode(",", $pic[1]);
  $pic_id = $params[0];
  $result = mysql_query("SELECT * FROM `pics` WHERE `id` = '".$pic_id."'");
  if($pic = mysql_fetch_assoc($result))
  {
    if($params[2]) {$default_width = $params[2];}
    if($params[1] == "l"){
      $style = ' style="float:left;padding:5px;"';
    }
    if($params[1] == "r"){
      $style = ' style="float:right;padding:5px;"';
    }
    if($params[1] == "c"){
      $style = ' style="text-align:center;padding:5px;width:100%;display:block;"';
    }
    if($pic['tooltip'] == "")
    {
      $tooltip = "no tooltip";
    }
    else
    {
      $tooltip = $pic['tooltip'];
    }
    $html_code = '
      <a'.$style.' class="pic_link" href="'.get_image_url_by_size_and_id($pic['id'],$default_width_lightbox).'" title="'.$pic['tooltip'].'" rel="lightbox[]"><img class="noretina" src="'.get_image_url_by_size_and_id($pic['id'],$default_width).'" alt="'.$tooltip.'">
      <img class="retina" src="'.get_image_url_by_size_and_id($pic['id'],($default_width*2)).'" alt="'.$tooltip.'"></a>
    ';
    return $html_code;
  }
  else
  {
    return '<img height="30" alt="Bild mit der id '.$pic_id.' kann nicht gefunden werden.">';
  }
}

function get_image_url_by_size_and_id($id,$size)
{
  $size_need = $size;
  $result = mysql_query("SELECT * FROM `pics` WHERE `id` = '".$id."'");
  $pic = mysql_fetch_assoc($result);
  if(!file_exists('images/'.$pic['name'].$size.'.jpg'))
  {
    resize_pic('images/'.$pic['name'],$size);

  }
  
  while(!file_exists('images/'.$pic['name'].$size.'.jpg') and $size > 0)
  {
    $size--;
  }
  
  if($size < $pic['size_x'] and $size_need >= $pic['size_x']) return 'images/'.$pic['name']; 
  else return 'images/'.$pic['name'].$size.'.jpg';
}

function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 1) .' KB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 1) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 1) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 1) .' TB';
    }
}

function customentities($string) { 
   return str_replace ( array ('<', '>'), array ( '&lt;' , '&gt;'), $string ); 
} 


function write_htaccess()
{
  $filename="header.txt"; //header
  
  

  $file=fopen($filename,"r");
  $header=fread($file,filesize($filename));
  fclose($file);
  $filename=".htaccess";
  
  if(is_writable($filename)){
    if(file_exists($filename))
      @unlink($filename);
    $file=fopen($filename,"w");
    fwrite($file,"# Diese Datei nicht bearbeiten, sie wird durch php neu erstellt!\r\n");
    fwrite($file,$header);
    fwrite($file,"RewriteEngine On\r\n");
    fwrite($file,"DirectoryIndex index.php?id=1\r\n");

    $htaccess_row="RewriteRule ^start$";
    $htaccess_row.=" index.php?seite=start\r\n";
    fwrite($file,$htaccess_row);
    $htaccess_row="RewriteRule ^intern$";
    $htaccess_row.=" index.php?seite=intern\r\n";
    fwrite($file,$htaccess_row);
    $htaccess_row="RewriteRule ^sitemap$";
    $htaccess_row.=" index.php?seite=sitemap\r\n";
    fwrite($file,$htaccess_row);
    $htaccess_row="RewriteRule ^help$";
    $htaccess_row.=" index.php?seite=help\r\n";
    fwrite($file,$htaccess_row);
    $htaccess_row="RewriteRule ^impressum$";
    $htaccess_row.=" index.php?seite=impressum\r\n";
    fwrite($file,$htaccess_row);

    $ergebnis = mysql_query("SELECT * FROM projekte ");
    while ($row = mysql_fetch_assoc($ergebnis))
    {
      $htaccess_row="RewriteRule ^".$row['file']."$";
      $htaccess_row.=" index.php?seite=".$row['file']."\r\n";
      if($row['file']!="")
        fwrite($file,$htaccess_row);
    }
    fclose($file);
    return ".htaccess wurde aktualisiert.";
  }else{
    return ".htaccess konnte nicht geschrieben werden. Keine Schreibrechte. -> sudo chown www-data .htaccess && sudo chmod 0774 .htaccess && sudo chgrp www-data .htaccess
";
  }
}

function get_param($zeile)
{
  $zeilen = file("param.txt");
  return $zeilen[$zeile];
}

function send_mail($empfaenger,$from,$subject,$mailtext){
  PostToHost("stay.moritzmair.de","/sendmail_post.php","stay.moritzmair.de/sendmail_post.php","empfaenger=".$empfaenger."&betreff=".$subject."&nachricht=".$mailtext."&header=".$from);
}

//post anfrage stellen
function PostToHost($host, $path, $referer, $data_to_send) {
  $fp = fsockopen($host, 80, $errno, $errstr);
  fputs($fp, "POST $path HTTP/1.1\r\n");
  fputs($fp, "Host: $host\r\n");
  fputs($fp, "Referer: $referer\r\n");
  fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
  fputs($fp, "Content-length: ". strlen($data_to_send) ."\r\n");
  fputs($fp, "Connection: close\r\n\r\n");
  fputs($fp, $data_to_send);
  while(!feof($fp)) {
      @$res .= fgets($fp, 128);
  }
  fclose($fp);
  return $res;
}


?>
