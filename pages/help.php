Die Fett makierten Codes werden auf jeder Seite entsprechend umgewandelt und k&ouml;nnen so zum Formatieren des Textes verwendet werden.

<table cellpadding="5">
	<tr>
		<td>Formatierung</td>
		<td>Code</td>
		<td>Beispiel</td>
		<td></td>
	</tr>
	<tr>
		<td>Fett</td>
		<td><b>[b]</b>text<b>[/b]</b></td>
		<td><b>text</b></td>
		<td></td>
	</tr>
	<tr>
		<td>Kursiv</td>
		<td><b>[i]</b>text<b>[/i]</b></td>
		<td><i>text</i></td>
		<td></td>
	</tr>
	<tr>
		<td>Unterstrichen</td>
		<td><b>[u]</b>text<b>[/u]</b></td>
		<td><u>text</u></td>
		<td></td>
	</tr>
	<tr>
		<td>&Uuml;berschrift</td>
		<td><b>[h]</b>text<b>[/h]</b></td>
		<td><h2>text</h2></td>
		<td></td>
	</tr>
	<tr>
		<td>Link</td>
		<td><b>[url=http://link.adresse.de]</b>text<b>[/url]</td>
		<td><a href="">text</a></td>
		<td></td>
	</tr>
	<tr>
		<td>HTML</td>
		<td><b>[html]</b>html Quellcode <?php echo htmlentities("<button>text</button>"); ?><b>[/html]</td>
		<td><button>text</button></td>
		<td></td>
	</tr>
	<tr>
		<td>Bilder</td>
		<td><b>[pic=id]</b> Wobei id der Bild id entspricht</td>
		<td>F&uuml;gt eben das entsprechende Bild ein.</td>
		<td></td>
	</tr>
	<tr>
		<td>kleine Bilder</td>
		<td><b>[pic=id,r,300]</b> r f&uuml;r rechts, l f&uuml;r links und c für mittig. Am Ende steht die Breite des Bildes</td>
		<td>F&uuml;gt das entsprechende Bild an der gew&uuml;nschten Stelle ein.</td>
		<td></td>
	</tr>
</table>
