<?php

if (isset($_GET['PHPSESSID']))
{
  $requesturi = preg_replace('/\?PHPSESSID=[^&]+/',"",$_SERVER['REQUEST_URI']);
  $requesturi = preg_replace('/&PHPSESSID=[^&]+/',"",$requesturi);
  header("HTTP/1.1 301 Moved Permanently");
  header("Location: http://".$_SERVER['HTTP_HOST'].$requesturi);
  exit;
}
ini_set("session.use_trans_sid", "0"); // schaltet die SID übergabe per GET Parameter AUS
ini_set("session.use_only_cookies", 1);

session_start();
$ladezeitzeit = microtime(); 
$ausgabe = "";
include "functions.php";
include "installsql/config.php";

if(!isset($_GET['seite'])) $_GET['seite'] = "";
if(!isset($_POST['name'])) $_POST['name'] = "";
if(!isset($_POST['mail'])) $_POST['mail'] = "";
if(!isset($_GET['logout'])) $_GET['logout'] = "";
if(!isset($_SESSION['drin'])) $_SESSION['drin'] = "";
if(!isset($_POST['login_check'])) $_POST['login_check'] = "";
if(!isset($_POST['projekt_check'])) $_POST['projekt_check'] = "";
if(!isset($_POST['profil_check'])) $_POST['profil_check'] = "";
if(!isset($_POST['intern_check'])) $_POST['intern_check'] = "";
if(!isset($_POST['comment_check'])) $_POST['comment_check'] = "";
if(!isset($_POST['pic_upload_check'])) $_POST['pic_upload_check'] = "";
if(!isset($_POST['check_new_pw'])) $_POST['check_new_pw'] = "";
if(!isset($zusatz_link)) $zusatz_link = "";

$error = "";
$links = "";
$projekt_active = "";

$wennnichdrin = "";


//mysql injection verhindern
$_GET['seite'] = str_replace("'", "", $_GET['seite']);
$_POST['name'] = str_replace("'", "", $_POST['name']);
$_POST['mail'] = str_replace("'", "", $_POST['mail']);


if($_GET['logout'] == 1)
{
  $_SESSION['drin'] = 0;
}

if($_SESSION['drin'] == 1)
{
  $client_ip = $_SERVER['REMOTE_ADDR'];
  mysql_query("UPDATE `users` SET `lastrefresh` = '".time()."' WHERE `id` = '".$_SESSION['user']."'")OR die(mysql_error());
  mysql_query("UPDATE `users` SET `ip` = '".$client_ip."' WHERE `id` = '".$_SESSION['user']."'")OR die(mysql_error());
}
 
if($_POST['login_check'] == 1)
{
  $result = mysql_query("SELECT * FROM `users` WHERE `name` = '".$_POST['name']."'");
  $user = mysql_fetch_assoc($result);
  if($user != "")
  {
    if($user['passwort'] == md5(trim($_POST['pw'])))
    {
      $_SESSION['drin'] = 1;
      $_SESSION['user'] = $user['id'];
    }
    else
    {
      $ausgabe[] = "Passwort falsch!";
    }
  }
  else
  {
    $ausgabe[] = "Benutzername falsch!";
  }
}

if($_POST['projekt_check'] == 1)
{
  if(trim($_POST['projekt_text'][0]) != "")
  {
    $result = mysql_query("SELECT * FROM `projekte` WHERE `id` = '".$_POST['projekt_id']."'");
    $projekt_save = mysql_fetch_assoc($result);
        
    if($projekt_save['text'] != "")
    {
      mysql_query("UPDATE `projekte` SET `changed` = '".time()."' WHERE `id` = '".$_POST['projekt_id']."'")OR die(mysql_error());
      mysql_query("UPDATE `projekte` SET `changedfrom` = '".$_SESSION['user']."' WHERE `id` = '".$_POST['projekt_id']."'")OR die(mysql_error());
    }
    mysql_query("UPDATE `projekte` SET `text` = '".preg_replace("/'/", "\'",trim($_POST['projekt_text'][0]))."' WHERE `id` = '".$_POST['projekt_id']."'")OR die(mysql_error());
    $ausgabe[] = write_htaccess();
    
  }
  else
  {
    $ausgabe[] = "Kein Text abgeschickt.";
  }
}

if($_POST['profil_check'] == 1)
{
  foreach($_POST as $key => $element)
  {
    if($key != "name") mysql_query("UPDATE `users` SET `".$key."` = '".htmlentities(trim($element))."' WHERE `id` = '".$_SESSION['user']."'");
  }
  $ausgabe[] = "Profil gespeichert.";
}

if($_POST['intern_check'] == 1)
{
  foreach($_POST as $key => $content)
  {
    if(is_array($content))
    {
      if(!isset($content['kill'])) $content['kill'] = "";
      if(!isset($content['name'])) $content['name'] = "";
      if($content['name'] != "")
      {
        $result = mysql_query("SELECT * FROM `projekte` WHERE `id` = '".$key."'");
        $projekt = mysql_fetch_assoc($result);
        
        if($projekt != "")
        {
          //Projekte verbergen bzw. einblenden
          $content['hide'] = isset($content['hide']) ? ($content['hide']) : false;
          if($content['hide'] == "on" and $projekt['hide'] != 1)
          {
            mysql_query("UPDATE `projekte` SET `hide` = '1' WHERE `id` = '".$key."'")OR die(mysql_error());
            $ausgabe[] = htmlentities($content['name'])." wurde erfolgreich verborgen.";
          }
          
          if($content['hide'] != "on" and $projekt['hide'] != 0)
          {
            mysql_query("UPDATE `projekte` SET `hide` = '0' WHERE `id` = '".$key."'")OR die(mysql_error());
            $ausgabe[] = htmlentities($content['name'])." wurde erfolgreich eingeblendet.";
          }
          unset($content['hide']);

          $content['kill'] = isset($content['kill']) ? ($content['kill']) : false;
          if($content['kill'] == "on")
          {
            mysql_query("DELETE FROM `projekte` WHERE `id` = '".$key."'") OR die(mysql_error());
            $ausgabe[] = $content['name']." wurde erfolgreich gelöscht.";
          }
          else
          {
            unset($content['kill']);
            foreach($content as $id => $spalte)
            {
              if($projekt[$id] != $spalte)
              {
                mysql_query("UPDATE `projekte` SET `".$id."` = '".$spalte."' WHERE `id` = '".$key."'");
						    $ausgabe[] = write_htaccess();
              }
            }
          }
        }
        else
        {         
          $filename = urlencode(umlaute(preg_replace("/ /",'_',$content['name'])));
          $result = mysql_query("SELECT * FROM `projekte` WHERE `file` = '".$filename."'");
          $projekt = mysql_fetch_assoc($result);
          if($projekt == ""){
            //Projekt neu anlegen
            mysql_query("insert into `projekte` (`name`,`file`,`topic`,`created`)
              VALUES ('".htmlentities(umhtml($content['name']))."','".$filename."','".$content['topic']."','".time()."')
            ");
				    $ausgabe[] = write_htaccess();
          }
        }
      }
      else
      {
        $ausgabe[] = "Einige Felder wurden nicht ausgefüllt, die dazu gehörigen Zeilen konnten daher nicht gespeichert werden.";
      }  
    }
  }
}

if($_POST['pic_upload_check'] == 1)
{
  foreach($_POST['tooltip'] as $key => $tooltip)
  {
    mysql_query("UPDATE `pics` SET `tooltip` = '".htmlentities(trim($tooltip))."' WHERE `id` = '".$key."'");
  }
  if(is_array($_POST['del_file']))
  foreach($_POST['del_file'] as $key => $del)
  {
    if($del == "on")
    {
      $result = mysql_query("SELECT * FROM `pics` WHERE `id` = '".$key."'");
      $del_pic = mysql_fetch_assoc($result);
      for($i = 2000;$i > 0;$i--)
      {
        if(file_exists('images/'.$del_pic['name'].$i.'.jpg'))
        {
          unlink('images/'.$del_pic['name'].$i.'.jpg');
        }
      }
      unlink('images/'.$del_pic['name']);
      mysql_query("DELETE FROM `pics` WHERE `id` = '".$key."'") OR die(mysql_error());
      $ausgabe[] = "Bild ".$key." geloescht.";
    }
  }
}

if($_POST['check_new_pw'] == 1){
  if($_POST['re_pw'] == $_POST['pw']){
    $result = mysql_query("SELECT * FROM `users` WHERE `id` = '".$_SESSION['user']."'");
    $user = mysql_fetch_assoc($result);
    if($user['passwort'] == md5($_POST['pw_old'])){
      mysql_query("UPDATE `users` SET `passwort` = '".md5($_POST['pw'])."' WHERE `id` = '".$_SESSION['user']."'")OR die(mysql_error());
      $ausgabe[] = "Passwort wurde geändert.";
    }
    else{
      $ausgabe[] = "Das alte Passwort wurde falsch eingegeben.";
    }
  }
  else{
    $ausgabe[] = "Neue Passwörter stimmen nicht über ein.";
  }
}
  

$seite = $_GET["seite"];
if($seite == "") $seite = "Startseite";
if($result = mysql_query("SELECT * FROM `projekte` WHERE `file` = '".urlencode($seite)."'")){
  $projekt_active = mysql_fetch_assoc($result);

  preg_match('#(?<=")[\w/_\-]+(\.png|\.jpg|\.JPG|\.jpeg|\.gif)(?=")#', $projekt_active['text'], $treffer);
}



 ?>
<!DOCTYPE html>
<html lang="de" prefix="og: http://ogp.me/ns#">
 <head>
  <title>Femoral CMS - <?php echo $projekt_active['name']; ?></title>
  <link rel="stylesheet" href="lib/normalize.css" media="all">
  <link rel="stylesheet" href="lib/style.css">
  <link rel="stylesheet" href="lib/lightbox/lightbox.css">
  <meta name="viewport" content="width=device-width, user-scalable=0" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <script type="text/javascript" src="lib/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="lib/lightbox/lightbox.js"></script>
  <script type="text/javascript" src="lib/lightbox/jquery.smooth-scroll.min.js"></script>
  <script type="text/javascript" src="lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="lib/js.js"></script>
  <script type="text/javascript" src="lib/modernizr.custom.js"></script>
	<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
  <link rel="shortcut icon" href="lib/pics/favicon.png"/>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
 
  <script>
    $(document).ready(function() {
        $(".zeile").each(function(i,elem) {        
                  
                  $(elem).find("td input[type='hidden']").attr('value', i);
              } );

        $("#table tbody").sortable({
            stop: function(event, ui) {
                $(".zeile").each(function(i,elem) {        
                  $(elem).find("td input[type='hidden']").attr('value', i);
              } );
            }
        });
        $( "#table tbody" ).disableSelection();
            
    });

<?php
if(get_param(0) == 1){
?>
		tinymce.init({
				selector: "textarea",
				language : 'de',
				plugins: [
				    "save advlist autolink lists link image charmap preview anchor",
				    "searchreplace visualblocks code fullscreen",
				    "insertdatetime media table contextmenu paste "
				],
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
				save_enablewhendirty: true
		});
<?php
}
?>


    </script>
 </head>
 <body>
 <div class="femoral_wrapper">
  <nav>
    <ul>
     <!-- begin links //-->
        <?php
        if($_SESSION['drin'] != 1)
        {
          $wennnichdrin = " and `text` != '' and `hide` = '0'";
        } else {
          $wennnichdrin = "";
        }
        $result = mysql_query("SELECT * FROM `projekte` WHERE `topic` = '0'".$wennnichdrin." ORDER BY `pos` ASC");
        if($result){

          while($projekt = mysql_fetch_assoc($result))
          {
            $links[$projekt['file']] = $projekt['name'];
          }
          if($links)
          {
            gen_links($links);
          }    
        }
        else {
          $ausgabe[] = "Es wurden noch keine Seiten angelegt.";
        }

        
     ?>
     
     </ul>
     <!-- LOGIN //-->       
             
             <?php if($_GET['seite'] != ""){$zusatz_link = "?seite=".$_GET['seite'];}
             
                   if($_SESSION['drin'] != 1)
                   {
              ?>
             <form id="form" style="display:none;position:relative;left:10px;" method="post" action="index.php<?php echo $zusatz_link; ?>"> 
              <input style="width:150px;" type="text" name="name" value="<?php echo $_POST['name']; ?>" placeholder="Name"/><br />
              <input style="width:150px;" type="password" name="pw" value="<?php echo $_POST['pw']; ?>" placeholder="Passwort"/><br />
              <input type="hidden" name="login_check" value="1"/>
              <input type="submit" value="Einloggen"/>
             </form>
             
             <div style="color:#f00;"><?php echo $error; ?></div>
            <?php
                 }
                 else
                 {
                   unset($links);
                   $links['intern'] = "Verwaltung";
                   $links['help'] = "Hilfe";
                   ?>
                    <ul>
                   <?php
                   gen_links($links);
                   ?>
                    </ul>
                   <?php
                 }
                 
            ?>
     <!-- end links //-->
   </nav>

    
  <div class="content">
<?php
if(is_array($ausgabe))
{
  echo '<div style="border:1px solid #FF0000;background-color:#ccc;padding:10px;margin-bottom:15px;border-radius:5px;">';
  foreach($ausgabe as $zeile)
  {
    echo htmlentities($zeile)."<br>\n";
  }
  echo '</div>';
  unset($ausgabe);
}

if($projekt_active != "")
{
  echo '<br><h1 style="display:inline;">'.$projekt_active['name'].'</h1><br><br>';
  
  if($_SESSION['drin'] == 1)
  {
    echo '<form method="post">';
    echo '<textarea class="article_textarea" name="projekt_text[]">'.customentities($projekt_active['text']).'</textarea>';

    echo '<input name="projekt_check" value="1" type="hidden"><input name="projekt_id" value="'.$projekt_active['id'].'" type="hidden"><input type="submit" value="Speichern"></form><br><br>Vorschau:<br><br>';
  }
  
  echo '<article>'.parse($projekt_active['text']).'</article>';
  
  $result = mysql_query("SELECT * FROM `projekte` WHERE `topic` = '".$projekt_active['id']."'");
  if($projekt_k = mysql_fetch_assoc($result))
  {
    echo '<br><br><hr>';
  }
  
}
else
{
  $toll = "pages/".$seite.".php";
  $a = file_exists($toll);

  if  ($a == true){include "pages/".$seite.".php";}
  if  ($seite=="") {echo "Keine Startseite vorhanden";}

  if ($a == false){echo "Keine Startseite vorhanden";}
}
?>
		
    <br>  
	     
   <?php
   if($_SESSION['drin'] == 1)
   {
     echo 'Eingeloggt (<a href="index.php?seite='.$seite.'&logout=1">Ausloggen</a>)';
   }
   else
   {
     echo '<a href="javascript:show_login();">login</a> ';
   }
   ?>
    <a href="sitemap">Sitemap</a> <a href="impressum">Impressum</a>
   </div>
  </div>
 </body>
</html>
