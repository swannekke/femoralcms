<?php header("Content-Type: application/rss+xml");
echo('<?xml version="1.0" encoding="utf-8"?>'); 

?>
<rss version="2.0">
  <channel>
    <title>Femoral - Projekte</title>
    <link>http://www.femoral.de</link>
    <description>Femoral ist eine private Platform fuer Bastel-, Elektronik- oder sonstige Projekte.</description>
    <language>de-de</language>
    <copyright>Moritz Mair, mail@moritzmair.de</copyright>
    <pubDate><?php echo date(DATE_RFC2822); ?></pubDate>
    
<?php
  include "functions.php";
  include "installsql/config.php";

  $result = mysql_query("SELECT * FROM `projekte` ORDER BY `created` DESC LIMIT 0, 15");
  while($post = mysql_fetch_assoc($result))
  {
    ?>
    <item>
      <title><?php echo htmlentities(umlaute($post['name'])); ?></title>
      <description><?php echo htmlentities(umlaute(cutStr(strip_tags(str_replace(array('[',']'), array('<','>'), $post['text'])),500))); ?></description>
      <link><?php echo htmlentities('http://www.femoral.de/index.php?seite='.$post['file']); ?></link>
      <author>mail@femoral.de (<?php echo htmlentities(umlaute(get_users($post['id']))) ?>)</author>

      <pubDate><?php echo date(DATE_RFC2822,$post['created']); ?></pubDate>
    </item>
    <?php    
  }
?> 

 
  </channel>
 
</rss>
